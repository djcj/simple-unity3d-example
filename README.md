# simple-unity3d-example
Small and simple Unity 3D test project (Unity 4.7.1 and 5.3.4).
It's not even a game. Its main purpose is to serve as a test program for [u2deb](https://github.com/darealshinji/UnityEngine2deb), decompilers or similar tools.
Compiled binaries as well as the source packages can be found in the release section.
